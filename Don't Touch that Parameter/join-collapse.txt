Join collapse

postgresql.conf:
listen_addresses = '*'

pg_ident.conf - empty

pg_hba.conf:
local   all             all                                     trust
host    all             all             0.0.0.0/0               trust
host    all             all             ::0/0                   trust

-- create example tables
DROP TABLE IF EXISTS a CASCADE;

CREATE TABLE a(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    type_id SMALLINT
);

DROP TABLE IF EXISTS b CASCADE;

CREATE TABLE b(
    id SERIAL NOT NULL PRIMARY KEY,
    type_id SMALLINT,
    a_id INT REFERENCES a(id),
    some_date CHAR(10)
);

CREATE INDEX b_a ON b(a_id);

-- populate tables with data
-- use setseed to have repeatable results
SELECT setseed(0.314159);

-- add 1M rows to a
INSERT INTO a(type_id)
SELECT (random()*10+1)::int FROM generate_series(1, 1000000) i;

-- add 2M rows to b
INSERT INTO b(type_id, a_id, some_date)
SELECT (random()*10+1)::int, i%1000000+1,  i::CHAR(10) FROM generate_series(1, 2000000) i;

-- get fresh statistics
ANALYSE a;
ANALYSE b;

-- join order
DROP TABLE IF EXISTS c CASCADE;

CREATE TABLE c(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    b_id BIGINT REFERENCES b(id)
);

DROP TABLE IF EXISTS type_data CASCADE;


CREATE TABLE type_data(
    id SMALLINT,
    some_data CHAR(100)
);

INSERT INTO c(b_id)
SELECT (i+123456)%200000+1 FROM generate_series(1, 2000000) i;

INSERT INTO type_data(id, some_data)
SELECT i, 'Data ' || i FROM generate_series(1,11) i;

ANALYZE c;
ANALYSE type_data;


EXPLAIN ANALYZE
SELECT a.id
FROM a
INNER JOIN type_data a_data ON a_data.id = a.type_id
INNER JOIN b ON b.a_id = a.id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN c ON c.b_id = b.id
WHERE a.id < 10;

/*
"Nested Loop  (cost=100.41..38419.13 rows=16 width=8) (actual time=14.075..315.837 rows=80 loops=1)"
"  Join Filter: (b.type_id = b_data.id)"
"  Rows Removed by Join Filter: 800"
"  ->  Nested Loop  (cost=100.41..38415.35 rows=16 width=10) (actual time=14.068..315.704 rows=80 loops=1)"
"        Join Filter: (a.type_id = a_data.id)"
"        Rows Removed by Join Filter: 800"
"        ->  Hash Join  (cost=100.41..38411.57 rows=16 width=12) (actual time=14.045..315.527 rows=80 loops=1)"
"              Hash Cond: (c.b_id = b.id)"
"              ->  Seq Scan on c  (cost=0.00..30811.00 rows=2000000 width=8) (actual time=0.043..154.095 rows=2000000 loops=1)"
"              ->  Hash  (cost=100.21..100.21 rows=16 width=16) (actual time=0.045..0.045 rows=18 loops=1)"
"                    Buckets: 1024  Batches: 1  Memory Usage: 9kB"
"                    ->  Nested Loop  (cost=0.85..100.21 rows=16 width=16) (actual time=0.012..0.038 rows=18 loops=1)"
"                          ->  Index Scan using a_pkey on a  (cost=0.42..8.56 rows=8 width=10) (actual time=0.002..0.003 rows=9 loops=1)"
"                                Index Cond: (id < 10)"
"                          ->  Index Scan using b_a on b  (cost=0.43..11.44 rows=2 width=10) (actual time=0.003..0.003 rows=2 loops=9)"
"                                Index Cond: (a_id = a.id)"
"        ->  Materialize  (cost=0.00..1.17 rows=11 width=2) (actual time=0.000..0.001 rows=11 loops=80)"
"              ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.007..0.007 rows=11 loops=1)"
"  ->  Materialize  (cost=0.00..1.17 rows=11 width=2) (actual time=0.000..0.001 rows=11 loops=80)"
"        ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.002..0.003 rows=11 loops=1)"
"Planning time: 0.462 ms"
"Execution time: 315.886 ms"
*/

SET join_collapse_limit = 1;

EXPLAIN ANALYZE
SELECT a.id
FROM c
INNER JOIN b ON b.id = c.b_id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN a ON a.id = b.a_id
INNER JOIN type_data a_data ON a_data.id = a.type_id
WHERE a.id < 10;

/*
"Hash Join  (cost=67516.16..188719.53 rows=16 width=8) (actual time=1510.016..2400.150 rows=80 loops=1)"
"  Hash Cond: (a.type_id = a_data.id)"
"  ->  Hash Join  (cost=67514.91..188718.06 rows=16 width=10) (actual time=1509.989..2400.076 rows=80 loops=1)"
"        Hash Cond: (b.a_id = a.id)"
"        ->  Hash Join  (cost=67506.25..181209.25 rows=2000000 width=4) (actual time=611.218..2242.524 rows=2000000 loops=1)"
"              Hash Cond: (b.type_id = b_data.id)"
"              ->  Hash Join  (cost=67505.00..153708.00 rows=2000000 width=6) (actual time=611.199..1855.435 rows=2000000 loops=1)"
"                    Hash Cond: (c.b_id = b.id)"
"                    ->  Seq Scan on c  (cost=0.00..30811.00 rows=2000000 width=8) (actual time=0.035..246.339 rows=2000000 loops=1)"
"                    ->  Hash  (cost=32739.00..32739.00 rows=2000000 width=10) (actual time=610.507..610.507 rows=2000000 loops=1)"
"                          Buckets: 131072  Batches: 32  Memory Usage: 3723kB"
"                          ->  Seq Scan on b  (cost=0.00..32739.00 rows=2000000 width=10) (actual time=0.005..270.230 rows=2000000 loops=1)"
"              ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.004..0.004 rows=11 loops=1)"
"                    Buckets: 1024  Batches: 1  Memory Usage: 9kB"
"                    ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.001..0.002 rows=11 loops=1)"
"        ->  Hash  (cost=8.56..8.56 rows=8 width=10) (actual time=0.006..0.006 rows=9 loops=1)"
"              Buckets: 1024  Batches: 1  Memory Usage: 9kB"
"              ->  Index Scan using a_pkey on a  (cost=0.42..8.56 rows=8 width=10) (actual time=0.002..0.002 rows=9 loops=1)"
"                    Index Cond: (id < 10)"
"  ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.011..0.011 rows=11 loops=1)"
"        Buckets: 1024  Batches: 1  Memory Usage: 9kB"
"        ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.007..0.008 rows=11 loops=1)"
"Planning time: 0.310 ms"
"Execution time: 2400.220 ms"
*/