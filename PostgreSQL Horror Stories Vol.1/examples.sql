-- create example tables
DROP TABLE IF EXISTS base_token CASCADE;

CREATE TABLE base_token(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    created_on TIMESTAMPTZ NOT NULL DEFAULT now()
);

DROP TABLE IF EXISTS token CASCADE;

CREATE TABLE token(
    id BIGINT NOT NULL PRIMARY KEY REFERENCES basetoken(id),
    name VARCHAR(64)
);

-- populate tables with data
-- use setseed to have repeatable results
SELECT setseed(0.314159);

-- add 300K rows to base_token
INSERT INTO base_token(id)
SELECT i FROM generate_series(1, 300000) i;

-- add 300K rows to token
INSERT INTO token(id, name)
SELECT i, md5(i::TEXT) FROM generate_series(1, 300000) i;

-- get fresh statistics
ANALYSE base_token;
ANALYSE token;

CREATE UNIQUE INDEX ix_token_name ON token(name);


EXPLAIN ANALYZE
SELECT t.id
FROM token t
INNER JOIN base_token bt ON bt.id = t.id 
WHERE t.name LIKE 'ABC';
/*
Nested Loop  (cost=0.84..16.89 rows=1 width=8) (actual time=0.039..0.039 rows=0 loops=1)
  ->  Index Scan using ix_token_name on token t  (cost=0.42..8.44 rows=1 width=8) (actual time=0.039..0.039 rows=0 loops=1)
        Index Cond: ((name)::text = 'ABC'::text)
        Filter: ((name)::text ~~ 'ABC'::text)
  ->  Index Only Scan using base_token_pkey on base_token bt  (cost=0.42..8.44 rows=1 width=8) (never executed)
        Index Cond: (id = t.id)
        Heap Fetches: 0
Planning time: 0.380 ms
Execution time: 0.074 ms
*/

EXPLAIN ANALYZE
SELECT t.id
FROM token t
INNER JOIN base_token bt ON bt.id = t.id 
WHERE t.name LIKE (SELECT 'ABC')::TEXT;

/*
Hash Join  (cost=8072.76..13834.76 rows=1500 width=8) (actual time=54.162..54.162 rows=0 loops=1)
  Hash Cond: (bt.id = t.id)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=32) (actual time=0.000..0.000 rows=1 loops=1)
  ->  Seq Scan on base_token bt  (cost=0.00..4622.00 rows=300000 width=8) (actual time=0.006..0.006 rows=1 loops=1)
  ->  Hash  (cost=8054.00..8054.00 rows=1500 width=8) (actual time=54.143..54.143 rows=0 loops=1)
        Buckets: 2048  Batches: 1  Memory Usage: 16kB
        ->  Seq Scan on token t  (cost=0.00..8054.00 rows=1500 width=8) (actual time=54.143..54.143 rows=0 loops=1)
              Filter: ((name)::text ~~ ($0)::text)
              Rows Removed by Filter: 300000
Planning time: 0.210 ms
Execution time: 54.182 ms
*/

EXPLAIN ANALYZE
SELECT t.id
FROM token t
INNER JOIN base_token bt ON bt.id = t.id 
WHERE t.name = (SELECT 'ABC')::TEXT;

/*
Nested Loop  (cost=0.86..16.91 rows=1 width=8) (actual time=0.012..0.012 rows=0 loops=1)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=32) (actual time=0.000..0.000 rows=1 loops=1)
  ->  Index Scan using ix_token_name on token t  (cost=0.43..8.45 rows=1 width=8) (actual time=0.012..0.012 rows=0 loops=1)
        Index Cond: ((name)::text = ($0)::text)
  ->  Index Only Scan using base_token_pkey on base_token bt  (cost=0.42..8.44 rows=1 width=8) (never executed)
        Index Cond: (id = t.id)
        Heap Fetches: 0
Planning time: 0.175 ms
Execution time: 0.032 ms
*/